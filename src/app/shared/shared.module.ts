import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from '@shtian/ng-pick-datetime';

import { DynamicFormModule } from './dynamic-form/dynamic-form.module';
import { WidgetComponent } from './widget/widget.component';
import { SettingsComponent } from './widget/settings/settings.component';
import { QuickDateFormComponent } from './quick-date-form/quick-date-form.component';
import { DateRangeFormComponent } from './date-range-form/date-range-form.component';
import { RangeFormComponent } from './range-form/range-form.component';
import { DelimitedFormComponent } from './delimited-form/delimited-form.component';
import { FillRunLsFormComponent } from './fill-run-ls-form/fill-run-ls-form.component';
import { StringPlusDateFormComponent } from './string-plus-date-form/string-plus-date-form.component';
import { DynamicQueryFormComponent } from './dynamic-query-form/dynamic-query-form.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ClarityModule,
        OwlNativeDateTimeModule,
        OwlDateTimeModule,
        DynamicFormModule
    ],
    declarations: [
        WidgetComponent,
        SettingsComponent,
        DateRangeFormComponent,
        FillRunLsFormComponent,
        RangeFormComponent,
        DelimitedFormComponent,
        QuickDateFormComponent,
        StringPlusDateFormComponent,
        DynamicQueryFormComponent
    ],
    // entryComponents: to be able to load these components dynamically
    entryComponents: [
        QuickDateFormComponent, DateRangeFormComponent, RangeFormComponent,
        DelimitedFormComponent, FillRunLsFormComponent,
        StringPlusDateFormComponent
    ],
    exports: [
        CommonModule,
        ClarityModule,
        FormsModule, ReactiveFormsModule,
        OwlNativeDateTimeModule, OwlDateTimeModule, DynamicFormModule,
        WidgetComponent, DateRangeFormComponent, FillRunLsFormComponent,
        RangeFormComponent, DelimitedFormComponent, QuickDateFormComponent,
        StringPlusDateFormComponent, DynamicQueryFormComponent
    ]
})
export class SharedModule { }
