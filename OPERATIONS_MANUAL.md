# Prerequisites
  - internet for `npm install` step
  - npm - https://www.npmjs.com/get-npm ( npm>=v6.7.0, and node >=v8.x )

# Install
A Makefile is provided inside the repository in order to ease the installation and use of the application.
```
git clone https://gitlab.cern.ch/bril/webmonitor.git
cd webmonitor
make install
```

# Proxy settings
In order to run `webmonitor` a proxy needs to be configured and then a tunnel established with the CMS network. In case your proxy is ready skip this section and see `Run webmonitor`.

The proxy can be configured automatically using the provided `Makefile` or manually. 

Currently the proxy can be set automatically for the Mozilla Firefox and Google Chrome browsers (tested with `python == 3.10.4`).
The `Makefile` uses the `config_proxy.py` to set the values for the proxy settings. Ιn order for the `Makefile` to configure the proxy settings, the browser which will be used (Mozilla Firefox or Google Chrome) must be provided and respectively Mozilla Firefox's Profile folder or for the Google Chrome browser the installation path. Mozilla's Firefox profile path contains the `installs.ini` and `profiles.ini` files and the installation path for Google Chrome contains its executable file. 

Usually the default value of Mozilla's Firefox profile path is:

`Windows:`  C:/Users/<user_name>/AppData/Roaming/Mozilla/Firefox/ (e.g C:/Users/diama/AppData/Roaming/Mozilla/Firefox/ )\
`Linux:`  ~/.mozilla/firefox/\
`MacOS:` /Users/<user_name>/Library/Application\ Support/Firefox/ (e.g /Users/diama/Library/Application\ Support/Firefox/ )

Usually the default value of Google's Chrome installation path is:

`Windows:`  "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"\
`Linux:`  /usr/bin/google-chrome\
`MacOS:` /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome

To configure the proxy for the Mozilla Firefox browser run:
```
make path=<firefox_profile_path> browser=firefox config_proxy
#e.g Windows: make path=C:/Users/<username>/AppData/Roaming/Mozilla/Firefox/ browser=firefox config_proxy
#    Linux: make path=~/.mozilla/firefox/ browser=firefox config_proxy
#    MacOS: make path=/Users/<username>/Library/Application\ Support/Firefox/ browser=firefox config_proxy
```
A `user.js` file containing all the proxy settings will be created inside the Firefox's current profile folder.

To reset the proxy settings inside the `user.js` to the default values for the Mozilla Firefox browser run:
```
make path=<firefox_profile_path> browser=firefox reset_proxy
#e.g Windows: make path=C:/Users/<username>/AppData/Roaming/Mozilla/Firefox/ browser=firefox reset_proxy
#    Linux: make path=~/.mozilla/firefox/ browser=firefox reset_proxy
#    MacOS: make path=/Users/<username>/Library/Application\ Support/Firefox/ browser=firefox reset_proxy
```

To configure the proxy for the Google's Chrome browser run:
```
make path=<chrome_install_path> browser=chrome config_proxy
#e.g Windows: make path="C:/Program Files (x86)/Google/Chrome/Application/chrome.exe" browser=chrome config_proxy
#    Linux: make path=/usr/bin/google-chrome browser=chrome config_proxy
#    MacOS: make path=/Applications/Google\ Chrome.app/Contents/MacOS/Google browser=chrome config_proxy
```
The above command will launch Google Chrome using a new profile session, with the all the required proxy settings. In order for the launch
to be successful no other Google Chrome windows must be open. The configured proxy will be valid only for the current session, and won't stay
active after the session is terminated.

In case you are using a different browser or your machine does not fulfill some of the prerequisites you can set up the proxy manually.

```
->search "proxy" 
   make sure the following values are set:
  ->network.proxy.type value 1
  ->network.proxy.socks value localhost
  ->network.proxy.socks_port value 10880
  ->network.proxy.socks_remote_dns value true
  ->network.proxy.no_proxies_on add your noproxy whitelist,e.g. localhost,127.0.0.1,.fr,.ch,.us,.gov,.edu
```

Set the above variables for your browser. 
In Firefox in Options -> Advanced -> Network -> Settings or in Chrome in Settings -> Show advanced settings -> Network -> Change proxy settings.

# Run webmonitor
First establish a tunnel with the CMS network.

If you are connected to the CERN network run:
```
ssh -D 10880 username@cmsusr
# cmsusr password
```

Otherwise:
```
ssh -t username@lxplus.cern.ch -L 10880:localhost:8089 
# lxplus password
ssh -D 8089 username@cmsusr.cern.ch
# cmsusr password
```
To serve the application run.
```
make
# open http://localhost:4200
```
The application will be served on localhost:4200.

# Build
```
npm run build
```

The above packages the application into `dist/` directory. Contents of the directory can be served by a web server.


# Deploy
1. Build the application with `npm run build`
2. Move contents of `dist/` directory to the area from where the application is supposed to be served
3. Modify `base` tag inside `index.html` to have correct base e.g. `/webmonitor-test/`

(with BRIL's current setup, application is served by NGINX running on srv-s2d16-22-01 from a directory `/var/www/html/webmonitor`. This is the directory where contents of `dist/` must go. And in this case the default `base` tag `/` must be replaced with `/webmonitor/`')

After uploading the new files to the directory, restarting the server is recommended.

# Web server configuration
In general the application can be served by any web server which can be configured to:
1. First trying to return files (with basic file type headers) by path
2. AND failing to locate files by path, serve `index.html` from application root

Current choice for BRIL's webmonitor is an NGINX server with a configuration snippet (at `/etc/nginx/nginx.conf`):
```
...
http {
...
    server {
        listen 80;
...
        location /webmonitor {
            alias /var/www/html/webmonitor;
            index index.html;
            try_files $uri $uri/ /index.html =404;
        }
...
```

# Database Server Configuration (CORS)
Normally widgets use a common `database.service.ts` to query the Elasticsearch instance, but in some cases a custom database service is used by a particular widget.<br>
In order to allow Elasticsearch to respond to requests from anyone, the Cross-Origin Resource Sharing (CORS) must be enabled in the ES server configuration as follows:
```
http.cors.enabled: true
http.cors.allow-origin: “*”
```   

# Database access configuration
In case Elasticsearch instances would be moved, application should be redeployed with updated default endpoint addresses. File `src/app/config.ts` contains pointers to the existing databases in the following format:
```
export const DATA_SOURCES = {
    'DEFAULT': {
        'endpoint': 'http://srv-s2d16-22-01.cms:9200'
    },
    'main_daq_monitoring': {
        'endpoint': 'http://srv-s2d16-22-01.cms:9200'
    },
    'analysis_store': {
        'endpoint': 'http://srv-s2d16-25-01.cms:9200'
    }
}
```

Then `endpoint` values should be updated accordingly, in case of relocation of database instances.
