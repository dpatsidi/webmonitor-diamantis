serve:
	npm run serve

install:
	npm install --force

build:
	npm run build

config_proxy:
	python proxy_config.py ${path} ${browser} config_proxy

reset_proxy:
	python proxy_config.py ${path} ${browser} reset