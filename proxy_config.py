import configparser
import subprocess
import platform
import os
import sys

# Launch a new Chrome profile session with the configured proxy
def chrome_proxy_session(path, local_OS):
    if local_OS == "Windows":
        subprocess.run([path, "^", "--proxy-server=socks5://localhost:10880"], shell=True)
    elif local_OS == "Linux" or local_OS == "Darwin":
        subprocess.run([path, "\\", "--proxy-server=socks5://localhost:10880"])
    else:
        print("OS is not recognized. Check OPERATIONS_MANUAL.md for more info.")

# Create user.js containing the proxy configuration inside the profile folder
def config_firefox_proxy(profile_path):
    network_proxy_type = 'user_pref("network.proxy.type", 1);\n'
    network_proxy_socks = 'user_pref("network.proxy.socks", "localhost");\n'
    network_proxy_socks_port = 'user_pref("network.proxy.socks_port", 10880);\n'
    network_proxy_remote_dns = 'user_pref("network.proxy.socks_remote_dns", true);\n'
    network_proxy_no_proxies_on = 'user_pref("network.proxy.no_proxies_on", "localhost,127.0.0.1,.fr,.ch,.us,.gov,.edu");\n'

    with open(profile_path, 'w') as f:
        f.write(network_proxy_type)
        f.write(network_proxy_socks)
        f.write(network_proxy_socks_port)
        f.write(network_proxy_remote_dns)
        f.write(network_proxy_no_proxies_on)

# Reset proxy configuration to default
def reset_firefox_proxy(profile_path):
    network_proxy_type = 'user_pref("network.proxy.type", 1);\n'
    network_proxy_socks = 'user_pref("network.proxy.socks", "");\n'
    network_proxy_socks_port = 'user_pref("network.proxy.socks_port", 0);\n'
    network_proxy_remote_dns = 'user_pref("network.proxy.socks_remote_dns", false);\n'
    network_proxy_no_proxies_on = 'user_pref("network.proxy.no_proxies_on", "");\n'

    with open(profile_path, 'w') as f:
        f.write(network_proxy_type)
        f.write(network_proxy_socks)
        f.write(network_proxy_socks_port)
        f.write(network_proxy_remote_dns)
        f.write(network_proxy_no_proxies_on)
'''
    The script will configure or reset the proxy settings needed
    for the BRIL webmonitor webapp to function. Currently it supports
    the Mozilla Firefox and Google Chrome browsers.
    
    @param argv: arguments[1:] passed to proxy_config.py
    argv[0]: path to Mozilla Firefox's user data or to Google Chrome's executable (chrome.exe)
        Usually the default location for Mozilla Firefox's user data is:
            Windows: C:/Users/<username>/AppData/Roaming/Mozilla/Firefox/ (e.g C:/Users/diama/AppData/Roaming/Mozilla/Firefox/ )
            Linux: ~/.mozilla/firefox/
            MacOS: /Users/<username>/Library/Application\ Support/Firefox/ (e.g /Users/diama/Library/Application\ Support/Firefox/)
        The exact location of the user's current profile can be provided which can be found using about:support in the browser.
        The path will look something like:
            Windows:  C:/Users/<username>/AppData/Roaming/Mozilla/Firefox/Profiles/xxxxxxxx.default-release
            Linux: ~/.mozilla/firefox/xxxxxxxx.default-release
            MacOS: /Users/<username>/Library/Application\ Support/Firefox/Profiles/xxxxxxxx.default-release

        Usually the default path for Google Chrome's executable is:
            Windows: "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"
            Linux: /usr/bin/google-chrome
            MacOS: /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome

    argv[1]: flag for specifying the browser
        Possible values:
        - firefox: when using Mozilla Firefox browser
        - chrome: when using Google Chrome browser

    argv[2]: flag for configuring the proxy settings
        Possible values:
        - config_proxy: configures the proxy settings 
        - reset: sets the proxy settings to the default values

    When using Mozilla Firefox browser the script will find the current
    user's profile name and create inside the profile's folder a user.js file
    containing all the proxy settings. Depending on the arguments provided to 
    the proxy_config.py the proxy settings can be set to the ones required
    by the CMS network for the BRIL machine or set to the default values.

    When using Google Chrome's browser the script will launch Chrome using a new
    profile with the configured proxy settings. In order for the new session to be
    launched any open Chrome windows must be closed. If the browser is afterwards terminated
    the session will end and the proxy settings are discarded.
'''
def main(argv):
    # Get provided arguments
    try:
        path = argv[0]
        browser = argv[1]
        proxy_flag = argv[2]
    except IndexError:
        print("Not enough arguments provided. Check OPERATIONS_MANUAL.md for more info.")
        return

    firefox_profile_path = ""
    local_OS = ""

    # Read installs.ini file containing the current profile for Mozilla Firefox
    if browser == "firefox":
        config = configparser.ConfigParser()
        config.read(path + 'installs.ini')

        config_dict = {section: dict(config[section]) for section in config.sections()}

        # Get the name of the current profile
        default_profile_path = ""
        for section in config_dict:
            if "default" in config_dict[section]:
                default_profile_path =  config_dict[section]["default"]
        # Create path of Firefox's current user path
        firefox_profile_path = os.path.join(path, default_profile_path, 'user.js')
    elif browser == "chrome":
        local_OS = platform.system() # Get local machine's operating system
    else:
        print("Browser is not recognized. Check OPERATIONS_MANUAL.md for more info.")
        return
    # Call the appropriate function for setting/resetting the proxy
    if proxy_flag == "config_proxy":
        if browser == "firefox":
            config_firefox_proxy(firefox_profile_path)
        elif browser == "chrome":
            chrome_proxy_session(path, local_OS)
    elif proxy_flag == "reset" and browser == "firefox":
        reset_firefox_proxy(firefox_profile_path)
    else:
        print("Proxy configuration flags are not recognized. Check OPERATIONS_MANUAL.md for more info.")

if __name__ == "__main__":
    main(sys.argv[1:])
